﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookService.Api.Models;

namespace BookService.Api.Data
{
    public class BookServiceApiContext : DbContext
    {
        public BookServiceApiContext (DbContextOptions<BookServiceApiContext> options)
            : base(options)
        {
        }

        public DbSet<BookService.Api.Models.Author> Author { get; set; }

        public DbSet<BookService.Api.Models.Book> Book { get; set; }
    }
}
